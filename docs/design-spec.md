# ProximaX Sirius ID Design Specification Document
v0.1, Thomas Tran
## Introduction

### Purpose

This document specified architecture and software design decisions for the ProximaX Sirius ID platform.

### Scope

This describes the software architecture and software design decisions for the implementation of the Sirius ID platform. The intended audience of this document is the developers, designers and software testers of the ProximaX Sirius ID platform.

### Design Goals

The ProximaX Sirius ID platform is an identity system built on the powerful ProximaX Sirius Chain infrastructure that enables the creation of self-sovereign identities with associated credentials and personal data that are owned and controlled by users. The following are principles guided the design of the ProximaX Sirius ID platform.

- Control
  Users must be able to control their identity. They can create or import identities from the ProximaX Sirius specified DID method. In the future, the ProximaX Sirius ID platform may support multiple DID methods implementation such as BTCR, Sovrin, and Uport.

- Access
  Users must have full access to their data within the ProximaX Sirius ID platform. They can manage the verifiable credentials of the identities issued by third parties as well as their self-issued credentials without relying on any intermediaries.

- Transparency
  All the systems and algorithms handing the identities within the ProximaX Sirius ID platform must be open and transparent.

- Persistence
  All the user identities must be long-lived within the ProximaX Sirius ID platform and users can keep all data through the system upgrade
  
- Portability
  Information and services about user identities must be transportable. It means the same user identities’ data can be replicated and synchronized across multiple device wallets.

- Interoperability
  ProximaX Sirius ID platform identities should be used widely and would need to be acceptable to various systems. For example, the identity created in ProximaX Sirius ID platform can be discovered from another decentralized identity service via the universal resolver.

- Consent
  User consent is required to access attestations or claims with granular access controls.

- Minimization
  Disclosure of the user identities’ data must be as few as possible. For example, the ProximaX Sirius ID platform should allow users to expose parts of their identity such as the age of the user instead of the full date of birth.

- Protection
  Users must have full rights on their identities.

## Platform Overview

The ProximaX Sirius ID platform is built around the ProximaX Sirius platform technologies. From a high-level perspective, the ProximaX Sirius ID platform (PSID platform) is composed of three components: PSID User-Agent, PSID Wallet and PSID Bridge.

### PSID User Agent

An agent is a software part, like a mobile, web or service application that allows users to interact with other identity owners through the PSID wallet and the communication protocols PSID Bridge. 

### PSID Wallet

The PSID wallet stores all the digital identities of its holder and all the information related to them such as cryptographic keys and credentials in secure storage. 

The PSID wallet is broken down into modules, include identity management, credential issuance, and verification, authentication and application sessions.

#### Identity Management

Users will be able to create identities or import existing ones using the [ProximaX Sirius ID method](did-method-spec.md).

The outcome of the creation of identity will be a new DID (or ProximaX Sirius ID), an implementation of the [DID (decentralized identifier)](https://w3c.github.io/did-core/#introduction) specification over the ProximaX Sirius Chain network. The ProximaX Sirius ID is a truly self-sovereign identifier based on the latest version of the DID scheme specified by the [W3C Credential Community Group](https://www.w3.org/community/credentials/). 

The ProximaX Sirius ID can be used to identify a person, organization or thing. It resolves to [ProximaX Sirius ID Document](did-document-spec.md) - [DID-Document](https://w3c.github.io/did-core/#did-document), a simple document in JSON_LD format that describes how to use the specific ProximaX Sirius ID.

#### Credential issuance and verification.

Users will be able to view, add and remove the Verifiable Credentials (or Credentials in short) to and from the PSID wallet. The PSID wallet user can self-issue credentials or accepts credentials issued by others.

Depends on the PSID User-Agent capabilities, the [ProximaX Sirius ID Verifiable Credentials](verifiable-credential-spec.md) might be stored on-chain or off-chain.

#### Authentication and application sessions.

In order to interact with applications or services, users need to request the user agent wallet. Application or service then issues the authentication request to the user agent wallet containing the application or service DID and the credentials it wants to receive from the user agent wallet. Users is prompted to select identity from the user agent wallet and accept what the application or service is asking for. Once the user accepted the prompt, a new session will be created and to be use for interactions between the application/service and the user agent wallet. 

Users will be able to request access or revoke one or more application sessions from the user agent wallet application list. 


### PSID Bridge

The PSID bridge provides the way for identity holders to communicate and interact with each other. The preferred implementation of the PSID bridge is using the WebSocket to exchange the predefined messages to ensure the interoperability between various implementations.



